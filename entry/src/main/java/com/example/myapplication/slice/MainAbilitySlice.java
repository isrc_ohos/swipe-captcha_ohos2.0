/*
 *    Copyright 2016 mcxtzhang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.example.myapplication.slice;

import com.example.myapplication.ResourceTable;
import com.huawei.swipecaptchaview.lib.RotateCaptchaView;
import com.huawei.swipecaptchaview.lib.SwipeCaptchaView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;

public class MainAbilitySlice extends AbilitySlice {
    SwipeCaptchaView swipeCaptchaView;
    RotateCaptchaView rotateCaptchaView;
    Button button;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        //根据id找到相应的控件
        swipeCaptchaView = (SwipeCaptchaView) findComponentById(ResourceTable.Id_swipeCaptchaView);
        rotateCaptchaView = (RotateCaptchaView) findComponentById(ResourceTable.Id_rotateCaptchaView);
        button = (Button) findComponentById(ResourceTable.Id_btn_change);

        //调用 setImageId 方法设置图片
        swipeCaptchaView.setImageId(ResourceTable.Media_pic01);
        rotateCaptchaView.setImageId(ResourceTable.Media_pic02);

        //每次滑动结束后会根据判定结果回调
        swipeCaptchaView.setOnCaptchaMatchCallback(new SwipeCaptchaView.OnCaptchaMatchCallback() {
            @Override
            public void matchSuccess(SwipeCaptchaView swipeCaptchaView) {
                new ToastDialog(getContext())
                        .setText(" 验证成功！")
                        .setAlignment(LayoutAlignment.CENTER)
                        .show();
            }

            @Override
            public void matchFailed(SwipeCaptchaView swipeCaptchaView) {
                new ToastDialog(getContext())
                        .setText(" 验证失败，请重新验证！")
                        .setAlignment(LayoutAlignment.CENTER)
                        .show();
            }
        });

        rotateCaptchaView.setOnCaptchaMatchCallback(new RotateCaptchaView.OnCaptchaMatchCallback() {
            @Override
            public void matchSuccess(RotateCaptchaView rotateCaptchaView) {
                new ToastDialog(getContext())
                        .setText(" 验证成功！")
                        .setAlignment(LayoutAlignment.CENTER)
                        .show();
            }

            @Override
            public void matchFailed(RotateCaptchaView rotateCaptchaView) {
                new ToastDialog(getContext())
                        .setText(" 验证失败，请重新验证！")
                        .setAlignment(LayoutAlignment.CENTER)
                        .show();
            }
        });

        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                swipeCaptchaView.createCaptcha();
                rotateCaptchaView.createCaptcha();
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
