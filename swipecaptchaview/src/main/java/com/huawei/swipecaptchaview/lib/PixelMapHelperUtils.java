package com.huawei.swipecaptchaview.lib;

import ohos.app.Context;
import ohos.global.resource.*;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.util.Optional;

public class PixelMapHelperUtils {
    //获取图片的 PixelMap
    public static PixelMap getPixelMap(Context context, int id) {
        try {
            ResourceManager manager = context.getResourceManager();
            String path = manager.getMediaPath(id);
            RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
            ImageSource.SourceOptions options = new ImageSource.SourceOptions();
            options.formatHint = "image/png";
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            Resource asset = assetManager.openRawFile();
            ImageSource source = ImageSource.create(asset, options);
            return Optional.ofNullable(source.createPixelmap(decodingOptions)).get();
        } catch (NotExistException | WrongTypeException | IOException e) {
            e.printStackTrace();
        }
        return getPixelMap(context, ResourceTable.Media_no_resource);
    }
}
