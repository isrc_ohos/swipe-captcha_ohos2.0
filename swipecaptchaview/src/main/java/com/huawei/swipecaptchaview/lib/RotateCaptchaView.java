package com.huawei.swipecaptchaview.lib;

import ohos.agp.components.*;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.util.Optional;
import java.util.Random;

public class RotateCaptchaView extends DependentLayout {
    private DependentLayout mLayout;
    private Image mImage;
    private Slider mSlider;
    private static final int SLIDER_HEIGHT = 90;
    //控件的宽高
    protected int mWidth;
    protected int mHeight;
    //验证码旋转块的半径
    private int mCaptchaRadius;
    private Random mRandom;
    private int randomDegree;

    //验证的误差允许值
    private float mMatchDeviation;
    //绘制旋转块的路径，遮罩
    private Path mPath;
    //画笔
    private Paint mPaint;

    public RotateCaptchaView(Context context) {
        this(context, null);
    }

    public RotateCaptchaView(Context context, AttrSet attrSet) {
        this(context, attrSet, "0");
    }

    public RotateCaptchaView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet, styleName);
    }

    public void setImageId(int id) {
        mImage.setPixelMap(id);
        initCaptcha();
    }

    public void setMatchDeviation(float deviation) {
        mMatchDeviation = deviation;
    }

    public void setCaptchaRadius(int radiusVp) {
        mCaptchaRadius = AttrHelper.vp2px(radiusVp, mLayout.getContext());
        createCaptcha();
    }


    private void init(Context context, AttrSet attrSet, String defStyleAttr) {
        //获取xml文件中的控件参数
        mHeight = getHeight();
        mWidth = getWidth();
        if (mWidth == 0) { //match_parent
            //获取系统屏幕宽度
            mWidth = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().width;
        }
        mCaptchaRadius = (mHeight - SLIDER_HEIGHT - AttrHelper.vp2px(20, context)) / 2;
        mMatchDeviation = AttrHelper.vp2px(3, context);
        for (int i = 0; i < attrSet.getLength(); i++) {
            Optional<Attr> attr = attrSet.getAttr(i);
            if (attr.isPresent()) {
                switch (attr.get().getName()) {
                    case "captchaRadius":
                        if (attr.get().getDimensionValue() * 2 < mHeight - SLIDER_HEIGHT) {
                            mCaptchaRadius = attr.get().getDimensionValue();
                        }
                        break;
                    case "matchDeviation":
                        mMatchDeviation = attr.get().getDimensionValue();
                        break;
                    case "right_margin":
                    case "left_margin":
                        mWidth -= attr.get().getDimensionValue();
                        break;
                    default:
                        break;
                }
            }
        }

        mLayout = this;
        mSlider = new Slider(context);

        mImage = new Image(context);
        LayoutConfig imageConfig = new LayoutConfig(mWidth, mHeight - SLIDER_HEIGHT);
        mImage.setLayoutConfig(imageConfig);
        mImage.setScaleMode(Image.ScaleMode.CLIP_CENTER);
        mImage.setPixelMap(ResourceTable.Media_no_resource);
        mLayout.addComponent(mImage);

        //slider 拖动条 //TODO: Slider 样式
        mSlider = new Slider(mLayout.getContext());
        mSlider.setWidth(mWidth);  //宽度
        mSlider.setHeight(SLIDER_HEIGHT);       //高度
        mSlider.setMarginTop(mHeight - SLIDER_HEIGHT);
        mSlider.setMinValue(0);      //进度最小值
        mSlider.setMaxValue(10000);  //进度最大值（没有设100是因为不够丝滑）
        mSlider.setProgressValue(0); //当前进度值
        mSlider.setProgressColor(Color.BLACK);   //进度颜色
        setSlideListener(); //监听器
        mLayout.addComponent(mSlider);

        initCaptcha();
    }

    private void initCaptcha() {
        //设置画笔
        mRandom = new Random(System.nanoTime());

        mPaint = new Paint();
        mPaint.setAntiAlias(true);   //抗锯齿
        mPaint.setDither(true);      //使位图进行有利的抖动的位掩码标志
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setMaskFilter(new MaskFilter(10, MaskFilter.Blur.SOLID));    //阴影

        mPath = new Path();

        //createMatchAnim();    //TODO: 动画
        createCaptcha();
    }

    public void createCaptcha() {
        if (mImage.getPixelMap() != null) {
            randomDegree = mRandom.nextInt(240) + 60;
            mSlider.setProgressValue(0);
            mSlider.setEnabled(true);
            mPaint.setMaskFilter(new MaskFilter(10, MaskFilter.Blur.SOLID));
            mImage.invalidate();
            mLayout.invalidate();
        }

        //绘制遮罩路径
        mPath.reset();
        mPath.addCircle(mWidth / 2f, (mHeight - SLIDER_HEIGHT) / 2f, mCaptchaRadius, Path.Direction.CLOCK_WISE);
        mPath.close();

        PixelMap pixelMap = mImage.getPixelMap();
        //根据图片的原宽度 和 控件宽度 算出缩放比例
        int originWidth = pixelMap.getImageInfo().size.width;
        int originHeight = pixelMap.getImageInfo().size.height;
        float ratioWidth = (float) mWidth / originWidth;
        float ratioHeight = (float) (mHeight - SLIDER_HEIGHT) / originHeight;
        //因为 CLIP_CENTER 有可能裁剪横边或竖边，这里更小的 ratio 是被裁剪的边，另一个就是真实的缩放比例
        float ratio = Math.max(ratioWidth, ratioHeight);

        //绘制边框（这部分不随着滑动而更新）
        mImage.addDrawTask((component, canvas) -> {
            canvas.drawPath(mPath, mPaint);
        });
        //绘制验证码
        mLayout.addDrawTask((component, canvas) -> {
            //根据滑动条的数值调整旋转角度
            canvas.rotate(mSlider.getProgress() * 360 / 10000 - randomDegree, mWidth / 2f, (mHeight - SLIDER_HEIGHT) / 2f);
            canvas.translate(0, 0);
            //获取图片的 PixelMapHolder
            PixelMapHolder pixelMapHolder = new PixelMapHolder(pixelMap);
            //根据路径裁剪canvas
            canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
            //画布缩放至跟图片缩放程度一致
            canvas.scale(ratio, ratio);
            if (ratio == ratioWidth) {
                //根据比例计算出垂直方向上由于 CLIP_CENTER 裁剪掉的图片的高度
                float heightErr = (originHeight * ratio - (mHeight - SLIDER_HEIGHT)) / 2;
                //绘制内容
                canvas.drawPixelMapHolder(pixelMapHolder, 0, -heightErr / ratio, mPaint);
            } else {
                //根据比例计算出水平方向上由于 CLIP_CENTER 裁剪掉的图片的宽度
                float widthErr = (originWidth * ratio - mWidth) / 2;
                //绘制内容
                canvas.drawPixelMapHolder(pixelMapHolder, -widthErr / ratio, 0, mPaint);
            }
        });
    }

    //设置滑动条的监听
    private void setSlideListener() {
        mSlider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                mLayout.invalidate();
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {
                if (onCaptchaMatchCallback != null) {
                    if (Math.abs(mSlider.getProgress() * 360 / 10000 - randomDegree) < mMatchDeviation) {
                        mPaint.setMaskFilter(null); //取消验证码滑块的阴影
                        mImage.invalidate();
                        slider.setProgressValue(randomDegree * 10000 / 360);
                        slider.setEnabled(false);
                        onCaptchaMatchCallback.matchSuccess(RotateCaptchaView.this);
                        //mSuccessAnim.start();
                    } else {
                        slider.setProgressValue(0);
                        onCaptchaMatchCallback.matchFailed(RotateCaptchaView.this);
                        //mFailAnim.start();
                    }
                }
            }
        });
    }

    //验证码验证的回调
    public interface OnCaptchaMatchCallback {
        void matchSuccess(RotateCaptchaView rotateCaptchaView);

        void matchFailed(RotateCaptchaView rotateCaptchaView);
    }

    private OnCaptchaMatchCallback onCaptchaMatchCallback;

    public OnCaptchaMatchCallback getOnCaptchaMatchCallback() {
        return onCaptchaMatchCallback;
    }

    public RotateCaptchaView setOnCaptchaMatchCallback(OnCaptchaMatchCallback onCaptchaMatchCallback) {
        this.onCaptchaMatchCallback = onCaptchaMatchCallback;
        return this;
    }
}
