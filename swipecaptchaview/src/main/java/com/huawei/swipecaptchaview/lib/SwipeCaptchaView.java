package com.huawei.swipecaptchaview.lib;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.util.Optional;
import java.util.Random;

import static com.huawei.swipecaptchaview.lib.DrawHelperUtils.drawPartCircle;

public class SwipeCaptchaView extends DependentLayout {
    private DependentLayout mLayout;
    private Image mImage;
    private Slider mSlider;
    private static final int SLIDER_HEIGHT = 90;
    //控件的宽高
    protected int mWidth;
    protected int mHeight;
    //验证码滑块的宽高
    private int mCaptchaWidth;
    private int mCaptchaHeight;
    //验证码的左上角(起点)的x y
    private int mCaptchaX;
    private int mCaptchaY;
    private Random mRandom;

    //验证的误差允许值
    private float mMatchDeviation;
    //绘制滑块的path，遮罩
    private Path mCaptchaPath;
    //画笔
    private Paint mCaptchaPaint;
    private Paint mMaskPaint;
    private Paint mSuccessPaint;
    //动画
    private AnimatorValue mFailAnim;
    private AnimatorValue mSuccessAnim;
    private int mSuccessAnimOffset; //成功动画的offset
    private Path mSuccessPath;  //成功后的path
    //flags
    private boolean isDrawMask; //是否绘制滑块（验证失败闪烁动画用）
    private boolean isShowSuccessAnim;  //是否显示验证成功的动画

    public SwipeCaptchaView(Context context) {
        this(context, null);
    }

    public SwipeCaptchaView(Context context, AttrSet attrSet) {
        this(context, attrSet, "0");
    }

    public SwipeCaptchaView(Context context, AttrSet attrSet, String defStyleAttr) {
        super(context, attrSet, defStyleAttr);
        init(context, attrSet, defStyleAttr);
    }

    public void setImageId(int id) {
        mImage.setPixelMap(id);
        initCaptcha();
    }

    public void setMatchDeviation(float deviation) {
        mMatchDeviation = deviation;
    }

    public void setCaptchaWidth(int widthVp) {
        mCaptchaWidth = AttrHelper.vp2px(widthVp, mLayout.getContext());
        createCaptcha();
    }

    public void setCaptchaHeight(int heightVp) {
        mCaptchaHeight = AttrHelper.vp2px(heightVp, mLayout.getContext());
        createCaptcha();
    }

    public void setCaptchaSize(int widthVp, int heightVp) {
        mCaptchaWidth = AttrHelper.vp2px(widthVp, mLayout.getContext());
        mCaptchaHeight = AttrHelper.vp2px(heightVp, mLayout.getContext());
        createCaptcha();
    }

    private void init(Context context, AttrSet attrSet, String defStyleAttr) {
        //获取xml文件中的控件参数
        mHeight = getHeight();
        mWidth = getWidth();
        if (mWidth == 0) { //match_parent
            //获取系统屏幕宽度
            mWidth = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().width;
        }
        int defaultSize = AttrHelper.vp2px(30, context);
        mCaptchaHeight = defaultSize;
        mCaptchaWidth = defaultSize;
        mMatchDeviation = AttrHelper.vp2px(3, context);
        for (int i = 0; i < attrSet.getLength(); i++) {
            Optional<Attr> attr = attrSet.getAttr(i);
            if (attr.isPresent()) {
                switch (attr.get().getName()) {
                    case "captchaHeight":
                        mCaptchaHeight = attr.get().getDimensionValue();
                        break;
                    case "captchaWidth":
                        mCaptchaWidth = attr.get().getDimensionValue();
                        break;
                    case "matchDeviation":
                        mMatchDeviation = attr.get().getDimensionValue();
                        break;
                    case "right_margin":
                    case "left_margin":
                        mWidth -= attr.get().getDimensionValue();
                    default:
                        break;
                }
            }
        }

        mLayout = this;
        mSlider = new Slider(context);

        mImage = new Image(context);
        LayoutConfig imageConfig = new LayoutConfig(mWidth, mHeight - SLIDER_HEIGHT);
        mImage.setLayoutConfig(imageConfig);
        mImage.setScaleMode(Image.ScaleMode.CLIP_CENTER);
        mImage.setPixelMap(ResourceTable.Media_no_resource);
        mLayout.addComponent(mImage);

        //slider 拖动条 //TODO: Slider 样式
        mSlider = new Slider(mLayout.getContext());
        mSlider.setWidth(mWidth);  //宽度
        mSlider.setHeight(SLIDER_HEIGHT);       //高度
        mSlider.setMarginTop(mHeight - SLIDER_HEIGHT);
        mSlider.setMinValue(0);      //进度最小值
        mSlider.setMaxValue(10000);  //进度最大值（没有设100是因为不够丝滑）
        mSlider.setProgressValue(0); //当前进度值
        mSlider.setProgressColor(Color.BLACK);   //进度颜色
        setSlideListener(); //监听器
        mLayout.addComponent(mSlider);
    }

    private void initCaptcha() {
        //设置画笔
        mRandom = new Random(System.nanoTime());

        mCaptchaPaint = new Paint();
        mCaptchaPaint.setAntiAlias(true);   //抗锯齿
        mCaptchaPaint.setDither(true);      //使位图进行有利的抖动的位掩码标志
        mCaptchaPaint.setStyle(Paint.Style.FILL_STYLE);
        mCaptchaPaint.setMaskFilter(new MaskFilter(10, MaskFilter.Blur.SOLID));    //阴影

        //滑块目标区域
        mMaskPaint = new Paint();
        mMaskPaint.setAntiAlias(true);
        mMaskPaint.setDither(true);
        mMaskPaint.setStyle(Paint.Style.FILL_STYLE);    //填充样式
        mMaskPaint.setColor(new Color(Color.argb(188, 0, 0, 0)));   //填充颜色
        mMaskPaint.setMaskFilter(new MaskFilter(20, MaskFilter.Blur.INNER));    //阴影

        mCaptchaPath = new Path();

        createMatchAnim();
        createCaptcha();
    }

    private void createMatchAnim() {
        //成功动画
        int width = AttrHelper.vp2px(60, mLayout.getContext());
        mSuccessAnim = new AnimatorValue();
        mSuccessAnim.setDuration(500);
        mSuccessAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                mSuccessAnimOffset = (int) (v * (mWidth + width));
                invalidate();
            }
        });
        mSuccessAnim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                isShowSuccessAnim = true;
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                isShowSuccessAnim = false;
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        mSuccessPaint = new Paint();
        mSuccessPaint.setShader(new LinearShader(
                new Point[]{new Point(0, 0), new Point(width * 3 / 2, mHeight)},
                new float[]{0, 0.5f},
                new Color[]{new Color(0x00FFFFFF), new Color(0x66FFFFFF)},
                Shader.TileMode.MIRROR_TILEMODE), Paint.ShaderType.LINEAR_SHADER);
        mSuccessPath = new Path();
        mSuccessPath.moveTo(0, 0);
        mSuccessPath.rLineTo(width, 0);
        mSuccessPath.rLineTo(width / 2, mHeight - SLIDER_HEIGHT);
        mSuccessPath.rLineTo(-width, 0);
        mSuccessPath.close();

        // 失败动画
        mFailAnim = new AnimatorValue();
        mFailAnim.setDuration(200);
        mFailAnim.setLoopedCount(2);
        mFailAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                isDrawMask = !(v < 0.5f);
                invalidate();
            }
        });
    }

    //生成验证码区域
    public void createCaptcha() {
        if (mImage.getPixelMap() != null) {
            createCaptchaPath();
            mSlider.setProgressValue(0);
            mSlider.setEnabled(true);
            isDrawMask = true;
            mCaptchaPaint.setMaskFilter(new MaskFilter(10, MaskFilter.Blur.SOLID));
            mLayout.invalidate();
        }

        PixelMap mCaptchaPixelMap = mImage.getPixelMap();//getPixelMap(mLayout.getContext(), ResourceTable.Media_pic01);
        //根据图片的原宽度 和 控件宽度 算出缩放比例
        int originWidth = mCaptchaPixelMap.getImageInfo().size.width;
        int originHeight = mCaptchaPixelMap.getImageInfo().size.height;
        float ratioWidth = (float) mWidth / originWidth;
        float ratioHeight = (float) (mHeight - SLIDER_HEIGHT) / originHeight;
        //因为 CLIP_CENTER 有可能裁剪横边或竖边，这里更小的 ratio 是被裁剪的边，另一个就是真实的缩放比例
        float ratio = Math.max(ratioWidth, ratioHeight);

        //滑块目标区域阴影的绘制（这部分不随着滑动条移动而更新）
        mImage.addDrawTask((component, canvas) -> {
            canvas.drawPath(mCaptchaPath, mMaskPaint);
        });
        //滑块区域的绘制
        mLayout.addDrawTask((component, canvas) -> {
            if (isShowSuccessAnim) {
                //绘制验证成功的动画
                canvas.translate(mSuccessAnimOffset, 0);
                canvas.drawPath(mSuccessPath, mSuccessPaint);
                canvas.translate(-mSuccessAnimOffset, 0);
            }
            if (isDrawMask) {
                //根据滑动条的数值计算画布的偏移量
                canvas.translate(mSlider.getProgress() * (mWidth - mCaptchaWidth) / 10000 - mCaptchaX, 0);
                //绘制边框
                canvas.drawPath(mCaptchaPath, mCaptchaPaint);
                //获取图片的 PixelMapHolder
                PixelMapHolder mCaptchaPixelMapHolder = new PixelMapHolder(mCaptchaPixelMap);
                //根据路径裁剪canvas
                canvas.clipPath(mCaptchaPath, Canvas.ClipOp.INTERSECT);
                //画布缩放至跟图片缩放程度一致
                canvas.scale(ratio, ratio);
                if (ratio == ratioWidth) {
                    //根据比例计算出垂直方向上由于 CLIP_CENTER 裁剪掉的图片的高度
                    float heightErr = (originHeight * ratio - (mHeight - SLIDER_HEIGHT)) / 2;
                    //绘制内容
                    canvas.drawPixelMapHolder(mCaptchaPixelMapHolder, 0, -heightErr / ratio, mCaptchaPaint);
                } else {
                    //根据比例计算出水平方向上由于 CLIP_CENTER 裁剪掉的图片的宽度
                    float widthErr = (originWidth * ratio - mWidth) / 2;
                    //绘制内容
                    canvas.drawPixelMapHolder(mCaptchaPixelMapHolder, -widthErr / ratio, 0, mCaptchaPaint);
                }
            }
        });
    }

    //绘制拼图快轮廓 Path
    private void createCaptchaPath() {
        //拼图缺口的位置，设置在中间 1/3 处
        int gap = mCaptchaWidth / 3;

        //随机生成验证码左上角的坐标
        mCaptchaX = mRandom.nextInt(mWidth - (mCaptchaWidth * 3) - gap) + (mCaptchaWidth * 2);
        mCaptchaY = mRandom.nextInt(mHeight - SLIDER_HEIGHT - mCaptchaHeight - gap);

        mCaptchaPath.reset();
        mCaptchaPath.lineTo(0, 0);
        //开始绘制图形
        mCaptchaPath.moveTo(mCaptchaX, mCaptchaY); //左上角
        mCaptchaPath.lineTo(mCaptchaX + gap, mCaptchaY);
        drawPartCircle(new Point(mCaptchaX + gap, mCaptchaY),
                new Point(mCaptchaX + gap * 2, mCaptchaY),
                mCaptchaPath, mRandom.nextBoolean());
        mCaptchaPath.lineTo(mCaptchaX + mCaptchaWidth, mCaptchaY);//右上角
        mCaptchaPath.lineTo(mCaptchaX + mCaptchaWidth, mCaptchaY + gap);
        drawPartCircle(new Point(mCaptchaX + mCaptchaWidth, mCaptchaY + gap),
                new Point(mCaptchaX + mCaptchaWidth, mCaptchaY + gap * 2),
                mCaptchaPath, mRandom.nextBoolean());
        mCaptchaPath.lineTo(mCaptchaX + mCaptchaWidth, mCaptchaY + mCaptchaHeight);//右下角
        mCaptchaPath.lineTo(mCaptchaX + mCaptchaWidth - gap, mCaptchaY + mCaptchaHeight);
        drawPartCircle(new Point(mCaptchaX + mCaptchaWidth - gap, mCaptchaY + mCaptchaHeight),
                new Point(mCaptchaX + mCaptchaWidth - gap * 2, mCaptchaY + mCaptchaHeight),
                mCaptchaPath, mRandom.nextBoolean());
        mCaptchaPath.lineTo(mCaptchaX, mCaptchaY + mCaptchaHeight);//左下角
        mCaptchaPath.lineTo(mCaptchaX, mCaptchaY + mCaptchaHeight - gap);
        drawPartCircle(new Point(mCaptchaX, mCaptchaY + mCaptchaHeight - gap),
                new Point(mCaptchaX, mCaptchaY + mCaptchaHeight - gap * 2),
                mCaptchaPath, mRandom.nextBoolean());
        mCaptchaPath.close();
    }

    //设置滑动条的监听
    private void setSlideListener() {
        mSlider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                mLayout.invalidate();
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {
                if (onCaptchaMatchCallback != null) {
                    if (Math.abs(mSlider.getProgress() * (mWidth - mCaptchaWidth) / 10000 - mCaptchaX) < mMatchDeviation) {
                        mCaptchaPaint.setMaskFilter(null); //取消验证码滑块的阴影
                        slider.setProgressValue(mCaptchaX * 10000 / (mWidth - mCaptchaWidth));
                        slider.setEnabled(false);
                        onCaptchaMatchCallback.matchSuccess(SwipeCaptchaView.this);
                        mSuccessAnim.start();
                    } else {
                        slider.setProgressValue(0);
                        onCaptchaMatchCallback.matchFailed(SwipeCaptchaView.this);
                        mFailAnim.start();
                    }
                }
            }
        });
    }

    //验证码验证的回调
    public interface OnCaptchaMatchCallback {
        void matchSuccess(SwipeCaptchaView swipeCaptchaView);

        void matchFailed(SwipeCaptchaView swipeCaptchaView);
    }

    private OnCaptchaMatchCallback onCaptchaMatchCallback;

    public OnCaptchaMatchCallback getOnCaptchaMatchCallback() {
        return onCaptchaMatchCallback;
    }

    public SwipeCaptchaView setOnCaptchaMatchCallback(OnCaptchaMatchCallback onCaptchaMatchCallback) {
        this.onCaptchaMatchCallback = onCaptchaMatchCallback;
        return this;
    }
}
