package com.huawei.swipecaptchaview.lib;

import ohos.agp.render.Path;
import ohos.agp.utils.Point;

public class DrawHelperUtils {

    /**
     * 传入起点、终点 坐标、凹凸和Path。
     * 会自动绘制凹凸的半圆弧
     *
     * @param start 起点坐标
     * @param end   终点坐标
     * @param path  半圆会绘制在这个path上
     * @param outer 是否凸半圆
     */
    public static void drawPartCircle(Point start, Point end, Path path, boolean outer) {
        float c = 0.551915024494f;
        //中点
        Point middle = new Point(start.getPointX() + (end.getPointX() - start.getPointX()) / 2,
                start.getPointY() + (end.getPointY() - start.getPointY()) / 2);
        //半径
        float r1 = (float) Math.sqrt(Math.pow((middle.getPointX() - start.getPointX()), 2) + Math.pow((middle.getPointY() - start.getPointY()), 2));
        //gap
        float gap1 = r1 * c;

        if (start.getPointX() == end.getPointX()) {
            //绘制竖直方向的

            //是否是从上到下
            boolean topToBottom = end.getPointY() - start.getPointY() > 0;
            //以下是我写出了所有的计算公式后推的，不要问我过程，只可意会。
            int flag;//旋转系数
            if (topToBottom) {
                flag = 1;
            } else {
                flag = -1;
            }
            if (outer) {
                //凸的 两个半圆
                path.cubicTo(start.getPointX() + gap1 * flag, start.getPointY(),
                        middle.getPointX() + r1 * flag, middle.getPointY() - gap1 * flag,
                        middle.getPointX() + r1 * flag, middle.getPointY());
                path.cubicTo(middle.getPointX() + r1 * flag, middle.getPointY() + gap1 * flag,
                        end.getPointX() + gap1 * flag, end.getPointY(),
                        end.getPointX(), end.getPointY());
            } else {
                //凹的 两个半圆
                path.cubicTo(start.getPointX() - gap1 * flag, start.getPointY(),
                        middle.getPointX() - r1 * flag, middle.getPointY() - gap1 * flag,
                        middle.getPointX() - r1 * flag, middle.getPointY());
                path.cubicTo(middle.getPointX() - r1 * flag, middle.getPointY() + gap1 * flag,
                        end.getPointX() - gap1 * flag, end.getPointY(),
                        end.getPointX(), end.getPointY());
            }
        }
    }
}
