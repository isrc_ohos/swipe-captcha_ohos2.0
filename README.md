# SwipeCaptcha_ohos

**本项目是基于开源项目SwipeCaptcha进行鸿蒙化的移植和开发的，可以通过项目标签以及github地址（https://github.com/mcxtzhang/SwipeCaptcha）追踪到原安卓项目版本。**

##### 项目介绍

- 项目名称：滑动验证码
- 所属系列：鸿蒙的第三方组件适配移植
- 功能：实现了滑动验证的功能，可以判断验证成功或失败
- 项目移植状态：100%，已完成全部的功能移植
- 调用差异：重构
- 开发版本：DevEco Studio 3.0 Beta1, SDK 5.0
- 项目发起作者：赵柏屹
- 邮箱：[isrc_hm@iscas.ac.cn](mailto:isrc_hm@iscas.ac.cn)
- 原项目Doc地址：https://github.com/mcxtzhang/SwipeCaptcha

![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/175901_06967ab5_4971371.gif "demo.gif")

##### 项目介绍

- 编程语言：Java
- 本项目为封装的第三方库，使用时需要将库导入项目。SwipeCaptchaView类继承了DependentLayout，使用时需在xml文件中以添加控件的方式添加至界面中。
- 拓展了一部份功能，添加了RotateCaptchaView类，用于实现旋转类型的滑动验证码。

##### 使用教程

1. 在xml文件中添加SwipeCaptchaView（或RotateCaptchaView）

   ```xml
   <!-- ability_main.xml -->
   <!--在这里添加 xmlns:app，为后面传入控件参数提供命名空间-->
   <DirectionalLayout
   	xmlns:app="http://schemas.huawei.com/res/ohos-auto">
       
   	<!--拼图验证码的参数可以从xml中设置，包括拼图的 宽 高 验证的阈值-->
       <!--这些参数也可以从 java 文件中动态设置-->
       <com.huawei.swipecaptchaview.lib.SwipeCaptchaView
           ohos:id="$+id:swipeCaptchaView"
           ohos:height="220vp"
           ohos:width="330vp"
           app:captchaHeight="30vp"
           app:captchaWidth="30vp"
           app:matchDeviation="9"/>
       
       <!--旋转验证码的参数也可从xml中设置，包括旋转块的 半径 验证的阈值-->
       <!--这些参数也可以从 java 文件中动态设置-->
       <com.huawei.swipecaptchaview.lib.RotateCaptchaView
           ohos:id="$+id:rotateCaptchaView"
           ohos:height="220vp"
           ohos:width="match_parent"
           ohos:left_margin="10vp"
           ohos:right_margin="10vp"
           app:captchaRadius="80vp"
           app:matchDeviation="8"/>
   </DirectionalLayout>
   ```

   注意：这里如果宽度定为`match_parent`，如再需要定义前后边距，必须使用 `left_margin / right_margin`，否则会不起作用。

2. 在 `AbilitySlice.java` 文件中绑定控件

   - SwipeCaptchaView

   ```java
   //MainAbilitySlice.java
   public class MainAbilitySlice extends AbilitySlice {
       SwipeCaptchaView swipeCaptchaView;
       
       @Override
       public void onStart(Intent intent) {
           super.onStart(intent);
           super.setUIContent(ResourceTable.Layout_ability_main);
           
           //根据 id 找到相应的控件
           swipeCaptchaView = (SwipeCaptchaView) findComponentById(ResourceTable.Id_swipeCaptchaView);
           //调用 setImageId 方法设置图片，如不设置图片，则会显示一张“暂无图片”内容的图片
           swipeCaptchaView.setImageId(ResourceTable.Media_pic01);
           //设置拼图块的宽高，这里的参数单位为 vp，与 xml 文件中的统一
           swipeCaptchaView.setCaptchaWidth(30);	//单独设置宽度
           swipeCaptchaView.setCaptchaHeight(30);	//单独设置高度
           swipeCaptchaView.setCaptchaSize(30, 30);//设置宽高
           //设置判定的误差允许值
           swipeCaptchaView.setMatchDeviation(9f);
           //绘制一张新的验证码（这个方法在绑定控件时会自动调用，第一次不需要调用该方法生成验证码，用于重新生成验证码）
           swipeCaptchaView.createCaptcha();
       }
   }
   ```

   - RotateCaptchaView

   ```java
   //MainAbilitySlice.java
   public class MainAbilitySlice extends AbilitySlice {
       RotateCaptchaView rotateCaptchaView;
       
       @Override
       public void onStart(Intent intent) {
           super.onStart(intent);
           super.setUIContent(ResourceTable.Layout_ability_main);
           
           //根据 id 找到相应的控件
           rotateCaptchaView = (RotateCaptchaView) findComponentById(ResourceTable.Id_rotateCaptchaView);
           //调用 setImageId 方法设置图片，如不设置图片，则会显示一张“暂无图片”内容的图片
           rotateCaptchaView.setImageId(ResourceTable.Media_pic02);
           //设置旋转块的半径，这里的参数单位为 vp，与 xml 文件中的统一
           rotateCaptchaView.setCaptchaRadius(80);
           //设置判定的误差允许角度
           rotateCaptchaView.setMatchDeviation(8f);
           //绘制一张新的验证码（这个方法在绑定控件时会自动调用，第一次不需要调用该方法生成验证码，用于重新生成验证码）
           rotateCaptchaView.createCaptcha();
       }
   }
   ```

3. 在  `AbilitySlice.java` 中处理回调函数

   ```java
   //MainAbilitySlice.java
   swipeCaptchaView.setOnCaptchaMatchCallback(new SwipeCaptchaView.OnCaptchaMatchCallback() {
   	@Override
       public void matchSuccess(SwipeCaptchaView swipeCaptchaView) {
           //添加验证成功时的代码
       }
   
       @Override
       public void matchFailed(SwipeCaptchaView swipeCaptchaView) {
           //添加验证失败时的代码
       }
   });
   ```

	rotateCaptchaView同理

##### 版本迭代

- v0.3.0-alpha

##### 版权和许可信息

- SwipeCaptcha_ohos经过[Apache License, version 2.0](http://www.apache.org/licenses/LICENSE-2.0)授权许可。
